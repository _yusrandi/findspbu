package com.kakocha.findpertamina;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.IdRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;
import com.kakocha.findpertamina.model.dataPertamina;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.String.valueOf;

public class DataDiriActivity extends AppCompatActivity {

    EditText etId, etAlamat, etPremium, etSolar, etPertamax, etPertalite, etPass;
    LinearLayout imgDone, imgBack;
    SwitchCompat scPremium, scPertamax, scPertalite, scSolar, scMushallah, scAtm, scToilet, scMinimarket, scBri, scBni, scBca, scMandiri, scMuamalat;
    private String minimarket, atm, masjid, toilet, id, alamat, premium, solar, pertamax, pertalite, password, bri, bni, bca, mandiri, muamalat;
    private String spbuLogin = MainActivity.idSpbu;
    LinearLayout lPremium;


    String url = UrlConfig.url + "update.php";
    String url_detail = UrlConfig.url + "tampildetail.php";
    String TAG = "DataDiriActivity";

    AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_diri);


        etAlamat = (EditText) findViewById(R.id.et_alamat);
        etId = (EditText) findViewById(R.id.et_id);
        etPertalite = (EditText) findViewById(R.id.et_pertalite);
        etPertamax = (EditText) findViewById(R.id.et_pertamax);
        etPremium = (EditText) findViewById(R.id.et_premium);
        etSolar = (EditText) findViewById(R.id.et_solar);
        etPass = (EditText) findViewById(R.id.et_pass);

        scPertalite = (SwitchCompat) findViewById(R.id.sc_pertalite);
        scPertamax = (SwitchCompat) findViewById(R.id.sc_pertamax);
        scPremium = (SwitchCompat) findViewById(R.id.sc_premium);
        scSolar = (SwitchCompat) findViewById(R.id.sc_solar);

        scMushallah = (SwitchCompat) findViewById(R.id.sc_mushallah);
        scMinimarket = (SwitchCompat) findViewById(R.id.sc_minimarket);
        scToilet = (SwitchCompat) findViewById(R.id.sc_toilet);
        scAtm = (SwitchCompat) findViewById(R.id.sc_atm);

        scBri = (SwitchCompat) findViewById(R.id.sc_bri);
        scBni = (SwitchCompat) findViewById(R.id.sc_bni);
        scBca = (SwitchCompat) findViewById(R.id.sc_bca);
        scMandiri = (SwitchCompat) findViewById(R.id.sc_mandiri);
        scMuamalat = (SwitchCompat) findViewById(R.id.sc_muamalat);

        imgDone = (LinearLayout) findViewById(R.id.img_done);
        imgBack = (LinearLayout) findViewById(R.id.img_back);

        lPremium = (LinearLayout) findViewById(R.id.layout_premium);

        builder = new AlertDialog.Builder(DataDiriActivity.this);

        etId.setEnabled(false);
        etAlamat.setEnabled(false);
        setData();

        if(scPremium != null){
            scPremium.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    Log.d(TAG, "The Switch is " + (b ? "on" : "off"));

                    if(b){
                        Log.d(TAG, "The Switch is on");
                        etPremium.setEnabled(true);
                        premium = etPremium.getText().toString();

                    }else {
                        Log.d(TAG, "The Switch is off");
                        etPremium.setEnabled(false);
                        premium = "0";
                    }
                }
            });
        }

        if(scPertamax != null){
            scPertamax.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    Log.d(TAG, "The Switch is " + (b ? "on" : "off"));

                    if(b){
                        Log.d(TAG, "The Switch is on");
                        etPertamax.setEnabled(true);
                        pertamax = etPertamax.getText().toString();

                    }else {
                        Log.d(TAG, "The Switch is off");
                        etPertamax.setEnabled(false);
                        pertamax = "0";
                    }
                }
            });
        }

        if(scPertalite != null){
            scPertalite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    Log.d(TAG, "The Switch is " + (b ? "on" : "off"));

                    if(b){
                        Log.d(TAG, "The Switch is on");
                        etPertalite.setEnabled(true);
                        pertalite = etPertalite.getText().toString();

                    }else {
                        Log.d(TAG, "The Switch is off");
                        etPertalite.setEnabled(false);
                        pertalite = "0";
                    }
                }
            });
        }

        if(scSolar != null){
            scSolar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    Log.d(TAG, "The Switch is " + (b ? "on" : "off"));

                    if(b){
                        Log.d(TAG, "The Switch is on");
                        etSolar.setEnabled(true);
                        solar = etSolar.getText().toString();

                    }else {
                        Log.d(TAG, "The Switch is off");
                        etSolar.setEnabled(false);
                        solar = "0";
                    }
                }
            });
        }

        if(scMushallah != null){
            scMushallah.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        masjid = "ya";
                    }else {
                        masjid = "tidak";
                    }
                }
            });
        }
        if(scToilet != null){
            scToilet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        toilet = "ya";
                    }else {
                        toilet = "tidak";
                    }
                }
            });
        }
        if(scAtm != null){
            scAtm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        atm = "ya";
                    }else {
                        atm = "tidak";
                    }
                }
            });
        }
        if(scMinimarket != null){
            scMinimarket.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        minimarket = "ya";
                    }else {
                        minimarket = "tidak";
                    }
                }
            });
        }

        if(scBri != null){
            scBri.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        bri = "ya";
                    }else {
                        bri = "tidak";
                    }
                }
            });
        }
        if(scBni != null){
            scBni.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        bni = "ya";
                    }else {
                        bni = "tidak";
                    }
                }
            });
        }
        if(scBca != null){
            scBca.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        bca = "ya";
                    }else {
                        bca = "tidak";
                    }
                }
            });
        }
        if(scMandiri != null){
            scMandiri.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        mandiri = "ya";
                    }else {
                        mandiri = "tidak";
                    }
                }
            });
        }
        if(scMuamalat != null){
            scMuamalat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        muamalat = "ya";
                    }else {
                        muamalat = "tidak";
                    }
                }
            });
        }

        imgDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData();
                updateData();
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    void getData() {


        id = etId.getText().toString();
        alamat = etAlamat.getText().toString();
        premium = etPremium.getText().toString();
        pertalite = etPertalite.getText().toString();
        pertamax = etPertamax.getText().toString();
        solar = etSolar.getText().toString();
        password = etPass.getText().toString();

        if(scPremium.isChecked()){
            premium = etPremium.getText().toString();
        }else premium = "0";
        if(scPertamax.isChecked()){
            pertamax = etPertamax.getText().toString();
        }else pertamax = "0";
        if(scPertalite.isChecked()){
            pertalite = etPertalite.getText().toString();
        }else pertalite = "0";
        if(scSolar.isChecked()){
            solar = etSolar.getText().toString();
        }else solar = "0";

        if(scAtm.isChecked()){
            atm = "ya";
        }else atm ="tidak";
        if(scMinimarket.isChecked()){
            minimarket = "ya";
        }else minimarket="tidak";
        if(scToilet.isChecked()){
            toilet="ya";
        }else toilet="tidak";
        if(scMushallah.isChecked()){
            masjid = "ya";
        }else masjid ="tidak";

        if(scBri.isChecked()){
            bri = "ya";
        }else bri = "tidak";
        if(scBni.isChecked()){
            bni = "ya";
        }else bni = "tidak";
        if(scBca.isChecked()){
            bca = "ya";
        }else bca = "tidak";
        if(scMandiri.isChecked()){
            mandiri = "ya";
        }else mandiri = "tidak";
        if(scMuamalat.isChecked()){
            muamalat = "ya";
        }else muamalat = "tidak";



    }

    void setData(){
        StringRequest request = new StringRequest(Request.Method.POST, url_detail, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d(TAG, response);
                try {

                    JSONArray data = new JSONArray(response);
                    if (data != null) {
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject jo = data.getJSONObject(i);

                            if (jo.getString("id").equals(spbuLogin)) {
                                etId.setText(jo.getString("id"));
                                etAlamat.setText(jo.getString("alamat"));
                                etPass.setText(jo.getString("password"));

                                Log.d(TAG, jo.getString("premium"));
                                if (jo.getInt("premium") != 0) {
                                    etPremium.setText(String.valueOf(jo.getInt("premium")));
                                    scPremium.setChecked(true);
                                } else {
                                    etPremium.setText("0");
                                    premium = "0";
                                    etPremium.setEnabled(false);
                                    scPremium.setChecked(false);
                                }
                                if (jo.getInt("pertamax") != 0) {
                                    etPertamax.setText(String.valueOf(jo.getInt("pertamax")));
                                    scPertamax.setChecked(true);
                                } else {
                                    etPertamax.setText("0");
                                    pertamax = "0";
                                    etPertamax.setEnabled(false);
                                    scPertamax.setChecked(false);
                                }
                                if (jo.getInt("pertalite") != 0) {
                                    etPertalite.setText(String.valueOf(jo.getInt("pertalite")));
                                    scPertalite.setChecked(true);
                                } else {
                                    etPertalite.setText("0");
                                    pertalite = "0";
                                    etPertalite.setEnabled(false);
                                    scPertalite.setChecked(false);
                                }
                                if (jo.getInt("solar") != 0) {
                                    etSolar.setText(String.valueOf(jo.getInt("solar")));
                                    scSolar.setChecked(true);
                                } else {
                                    solar = "0";
                                    etSolar.setText("0");
                                    etSolar.setEnabled(false);
                                    scSolar.setChecked(false);
                                }

                                if (jo.getString("mushallah").equalsIgnoreCase("ya")) {
                                    scMushallah.setChecked(true);
                                } else scMushallah.setChecked(false);

                                if (jo.getString("toilet").equalsIgnoreCase("ya")) {
                                    scToilet.setChecked(true);
                                } else scToilet.setChecked(false);

                                if (jo.getString("atm").equalsIgnoreCase("ya")) {
                                    scAtm.setChecked(true);
                                } else scAtm.setChecked(false);

                                if (jo.getString("minimarket").equalsIgnoreCase("ya")) {
                                    scMinimarket.setChecked(true);
                                } else scMinimarket.setChecked(false);

                                if (jo.getString("bri").equalsIgnoreCase("ya")) {
                                    scBri.setChecked(true);
                                } else scBri.setChecked(false);

                                if (jo.getString("bni").equalsIgnoreCase("ya")) {
                                    scBni.setChecked(true);
                                } else scBni.setChecked(false);

                                if (jo.getString("bca").equalsIgnoreCase("ya")) {
                                    scBca.setChecked(true);
                                } else scBca.setChecked(false);

                                if (jo.getString("muamalat").equalsIgnoreCase("ya")) {
                                    scMuamalat.setChecked(true);
                                } else scMuamalat.setChecked(false);

                                if (jo.getString("mandiri").equalsIgnoreCase("ya")) {
                                    scMandiri.setChecked(true);
                                } else scMandiri.setChecked(false);



                            }
                        }
                    }
                    } catch(JSONException e){
                        e.printStackTrace();
                    }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        MySingleton.getInstance(DataDiriActivity.this).addToRequestQueue(request);

    }
    void updateData() {
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);

                    String code = jsonObject.getString("code");
                    String message = jsonObject.getString("message");

                    builder.setTitle("Succesfully Update");
                    builder.setMessage(message);
                    displayAlert(code);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();


                Log.d(TAG, masjid + minimarket + id+ alamat + premium + pertamax+ pertalite+ solar+ masjid+ atm+minimarket+ password+"update data");

                params.put("id", id);
                params.put("alamat", alamat);
                params.put("premium", premium);
                params.put("pertamax", pertamax);
                params.put("pertalite", pertalite);
                params.put("solar", solar);
                params.put("mushallah", masjid);
                params.put("atm", atm);
                params.put("toilet", toilet);
                params.put("minimarket", minimarket);
                params.put("password", password);
                params.put("bri", bri);
                params.put("bni", bni);
                params.put("bca", bca);
                params.put("mandiri", mandiri);
                params.put("muamalat", muamalat);

                return params;

            }
        };
        MySingleton.getInstance(DataDiriActivity.this).addToRequestQueue(request);
    }

    void displayAlert(final String code) {
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (code.equals("update_success")) {
                    finish();
                    startActivity(new Intent(DataDiriActivity.this, MainActivity.class));
                }else if(code.equals("update_failed")){
                    builder.setTitle(code);
                    builder.setMessage("your ID or password is wrong");
                }
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


}
