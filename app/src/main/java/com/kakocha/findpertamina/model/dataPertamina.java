package com.kakocha.findpertamina.model;

import java.io.Serializable;

/**
 * Created by userundie on 20/10/17.
 */

public class dataPertamina implements Serializable{
    private String id;
    private String alamat;
    private double jarak;
    private double liter;
    private String latitude;
    private String longitude;
    private int premium;
    private int pertamax;
    private int pertalite;
    private int solar;
    private String mushallah;
    private String atm;
    private String toilet;
    private String pompa;
    private String password;

    private String bri, bni, bca, mandiri, muamalat;



    public dataPertamina(String id, String alamat, double jarak, double liter, String latitude, String longitude, int premium, int pertamax, int pertalite, int solar, String mushallah, String atm, String toilet, String pompa, String bri, String bni, String bca, String mandiri, String muamalat) {
        this.id = id;
        this.alamat = alamat;
        this.jarak = jarak;
        this.liter = liter;
        this.latitude = latitude;
        this.longitude = longitude;
        this.premium = premium;
        this.pertamax = pertamax;
        this.pertalite = pertalite;
        this.solar = solar;
        this.mushallah = mushallah;
        this.atm = atm;
        this.toilet = toilet;
        this.pompa = pompa;
        this.bri = bri;
        this.bni = bni;
        this.bca = bca;
        this.mandiri = mandiri;
        this.muamalat = muamalat;
    }

    public dataPertamina(String id, String alamat, int premium, int pertamax, int pertalite, int solar, String mushallah, String atm, String toilet, String pompa, String password) {
        this.id = id;
        this.alamat = alamat;
        this.premium = premium;
        this.pertamax = pertamax;
        this.pertalite = pertalite;
        this.solar = solar;
        this.mushallah = mushallah;
        this.atm = atm;
        this.toilet = toilet;
        this.pompa = pompa;
        this.password = password;
    }

    public String getId() {
        return id;
    }


    public String getAlamat() {
        return alamat;
    }

    public double getJarak() {
        return jarak;
    }

    public double getLiter() {
        return liter;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public int getPremium() {
        return premium;
    }

    public int getPertamax() {
        return pertamax;
    }

    public int getPertalite() {
        return pertalite;
    }

    public int getSolar() {
        return solar;
    }

    public String getMushallah() {
        return mushallah;
    }

    public String getAtm() {
        return atm;
    }

    public String getToilet() {
        return toilet;
    }

    public String getPompa() {
        return pompa;
    }

    public String getBri() {
        return bri;
    }

    public String getBni() {
        return bni;
    }

    public String getBca() {
        return bca;
    }

    public String getMandiri() {
        return mandiri;
    }

    public String getMuamalat() {
        return muamalat;
    }
}
