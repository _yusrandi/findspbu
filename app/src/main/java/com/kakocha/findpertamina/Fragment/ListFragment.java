package com.kakocha.findpertamina.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;
import com.kakocha.findpertamina.Adapter.PertaminaAdapter;
import com.kakocha.findpertamina.MySingleton;
import com.kakocha.findpertamina.R;
import com.kakocha.findpertamina.UrlConfig;
import com.kakocha.findpertamina.model.dataPertamina;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Demonstrates the use of {@link RecyclerView} with a {@link LinearLayoutManager} and a
 * {@link GridLayoutManager}.
 */
public class ListFragment extends Fragment{

    final String TAG = "MainActivity";
    ArrayList<dataPertamina> listPertamina = new ArrayList<>();
    RecyclerView recyclerView;
    View mView;
    double longPos, latPos;
    int jarak, liter;
    double Distance, Liter;
    DecimalFormat df = new DecimalFormat("#.#");
    ProgressDialog loading;
    public ListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView =  inflater.inflate(R.layout.fragment_list, container, false);
        // Inflate the layout for this fragment




        String url = UrlConfig.url+"tampildetail.php";
        longPos = MapsFragment.longPos;
        latPos = MapsFragment.latPos;
        loading = ProgressDialog.show(getContext(), "Loading data", "Please wait...", false, false);

        Log.d(TAG, String.valueOf(longPos)+"ini mi static longpos");
        Log.d(TAG, String.valueOf(latPos)+"ini mi static latpos");
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try {
                    recyclerView = (RecyclerView) mView.findViewById(R.id.my_recyclerView);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setNestedScrollingEnabled(false);

                    LinearLayoutManager manager = new LinearLayoutManager(getContext());

                    recyclerView.setLayoutManager(manager);


                    JSONArray data = new JSONArray(response);

                    loading.dismiss();
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject jo = data.getJSONObject(i);

                        LatLng from = new LatLng(latPos,longPos);
                        LatLng to = new LatLng(Double.valueOf(jo.getString("latitude")),Double.valueOf(jo.getString("longitude")));

                        //Calculating the distance in meters
                        Double distance = SphericalUtil.computeDistanceBetween(from, to);
                        Distance = (distance*0.003)/2;

                        Liter = (Distance * 0.1);

                        dataPertamina mDataPertamina = new dataPertamina(jo.getString("id"),jo.getString("alamat"), Double.valueOf(df.format(Distance)), Double.valueOf(df.format(Liter)), jo.getString("latitude"), jo.getString("longitude"), jo.getInt("premium"), jo.getInt("pertamax"), jo.getInt("pertalite"), jo.getInt("solar"), jo.getString("mushallah"), jo.getString("atm"), jo.getString("toilet"), jo.getString("minimarket"), jo.getString("bri"), jo.getString("bni"), jo.getString("bca"), jo.getString("muamalat"), jo.getString("mandiri"));
                        listPertamina.add(mDataPertamina);

                        PertaminaAdapter adapter = new PertaminaAdapter(getContext(), listPertamina, recyclerView);
                        recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();



                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
                Toast.makeText(getContext(), "periksa koneksi anda.", Toast.LENGTH_LONG).show();
            }
        });
        MySingleton.getInstance(getContext()).addToRequestQueue(request);



        return mView;
    }

    @Override
    public void onResume() {

        super.onResume();
    }
}
