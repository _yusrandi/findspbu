package com.kakocha.findpertamina.Fragment;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.identity.intents.Address;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;
import com.kakocha.findpertamina.Adapter.PertaminaAdapter;
import com.kakocha.findpertamina.MySingleton;
import com.kakocha.findpertamina.R;
import com.kakocha.findpertamina.UrlConfig;
import com.kakocha.findpertamina.model.dataPertamina;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.zip.Inflater;

import static java.lang.String.valueOf;

/**
 * Demonstrates the use of {@link RecyclerView} with a {@link LinearLayoutManager} and a
 * {@link GridLayoutManager}.
 */
public class MapsFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    GoogleMap googleMap;
    MapView mapView;
    View mView;
    final String TAG = "MainActivity";
    ArrayList<dataPertamina> listPertamina = new ArrayList<>();
    Dialog dialog;
    dataPertamina mDataPertamina;
    LocationManager locationManager;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;

    LatLng latLng;
    SupportMapFragment mFragment;
    Marker currLocationMarker;

    public static double latPos;
    public static double longPos;
    private double toLatPos, toLongPos, Distance, Liter ;
    ProgressDialog loading;


    public MapsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_maps, container, false);
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pop_up_detail);

        loading = ProgressDialog.show(getContext(), "Loading data", "Please wait...", false, false);

        parseData();


        return mView;
    }

    private void parseData() {
        final DecimalFormat df = new DecimalFormat("#.#");
        String url = UrlConfig.url + "tampildetail.php";
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try {

                    JSONArray data = new JSONArray(response);

                    for (int i = 0; i < data.length(); i++) {
                        JSONObject jo = data.getJSONObject(i);


                        LatLng from = new LatLng(latPos,longPos);
                        LatLng to = new LatLng(toLatPos,toLongPos);

                        //Calculating the distance in meters
                        Double distance = SphericalUtil.computeDistanceBetween(from, to);
                        Distance = (distance*0.003)/2;

                        Liter = (Distance * 0.10);
                        Log.d(TAG, valueOf(longPos) + "parseData");

                        mDataPertamina = new dataPertamina(jo.getString("id"), jo.getString("alamat"), Double.valueOf(df.format(Distance)), Double.valueOf(df.format(Liter)), jo.getString("latitude"), jo.getString("longitude"), jo.getInt("premium"), jo.getInt("pertamax"), jo.getInt("pertalite"), jo.getInt("solar"), jo.getString("mushallah"), jo.getString("atm"), jo.getString("toilet"), jo.getString("minimarket"), jo.getString("bri"), jo.getString("bni"), jo.getString("bca"), jo.getString("muamalat"), jo.getString("mandiri"));
                        listPertamina.add(mDataPertamina);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
                Toast.makeText(getContext(), "Periksa koneksi anda.", Toast.LENGTH_LONG).show();
            }
        });
        MySingleton.getInstance(getContext()).addToRequestQueue(request);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mapView = mView.findViewById(R.id.maps);
        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }

    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {

        MapsInitializer.initialize(getContext());
        Log.d(TAG, "onMap Ready");
        this.googleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);

        buildGoogleApiClient();

        mGoogleApiClient.connect();
//        CameraPosition Liberty = CameraPosition.builder().target(new LatLng(-5.074196, 119.522641)).zoom(15).bearing(0).tilt(45).build();
//        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(Liberty));
//
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                LatLng latLng = marker.getPosition();

                WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                Window window = dialog.getWindow();
                layoutParams.copyFrom(window.getAttributes());
                layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                window.setAttributes(layoutParams);

                TextView tvId, tvAlamat, tvJarak, tvLiter, tvPremium, tvPertamax, tvPertalite, tvSolar;
                LinearLayout mMasjid, mAtm, mWc, mMini;
                Button btnGo;

                tvId = (TextView) dialog.findViewById(R.id.tv_id);
                tvAlamat = (TextView) dialog.findViewById(R.id.tv_alamat);
                tvJarak = (TextView) dialog.findViewById(R.id.tv_jarak);
                tvLiter = (TextView) dialog.findViewById(R.id.tv_liter);
                tvPremium = (TextView) dialog.findViewById(R.id.tv_det_Premium);
                tvPertalite = (TextView) dialog.findViewById(R.id.tv_det_pertalite);
                tvPertamax = (TextView) dialog.findViewById(R.id.tv_det_pertamax);
                tvSolar = (TextView) dialog.findViewById(R.id.tv_det_solar);
                btnGo = dialog.findViewById(R.id.btn_go_detail);

                mMasjid = dialog.findViewById(R.id.img_masjid);
                mAtm = dialog.findViewById(R.id.img_atm);

                mWc = dialog.findViewById(R.id.img_wc);
                mMini = dialog.findViewById(R.id.img_mini);

                DecimalFormat df = new DecimalFormat("#.#");

                for(final dataPertamina mData : listPertamina ){
                    if(latLng.latitude == Double.valueOf(mData.getLatitude())){
                        toLongPos = Double.valueOf(mData.getLongitude());
                        toLatPos = Double.valueOf(mData.getLatitude());



                        tvId.setText(mData.getId());
                        tvAlamat.setText(mData.getAlamat());

                        if(mData.getPremium()==0){
                            tvPremium.setText("Tidak tersedia");
                        }else{
                            tvPremium.setText("Rp. " + valueOf(mData.getPremium()));
                        }

                        if(mData.getPertamax()==0){
                            tvPertamax.setText("Tidak tersedia");
                        }else{
                            tvPertamax.setText("Rp. " + valueOf(mData.getPertamax()));
                        }

                        if(mData.getPertalite()==0){
                            tvPertalite.setText("Tidak tersedia");
                        }else{
                            tvPertalite.setText("Rp. " + valueOf(mData.getPertalite()));
                        }

                        if(mData.getSolar()==0){
                            tvSolar.setText("Tidak tersedia");
                        }else{
                            tvSolar.setText("Rp. " + valueOf(mData.getSolar()));
                        }

                        LatLng from = new LatLng(latPos,longPos);
                        LatLng to = new LatLng(toLatPos,toLongPos);

                        //Calculating the distance in meters
                        Double distance = SphericalUtil.computeDistanceBetween(from, to);
                        Distance = (distance*0.003)/2;

                        Liter = (Distance * 0.1);

                        tvJarak.setText(valueOf(df.format(Distance)) + " KM");

                        tvLiter.setText(valueOf(df.format(Liter)) + " L");

                        if(mData.getMushallah().equalsIgnoreCase("tidak")){
                            mMasjid.setVisibility(View.GONE);
                        }else mMasjid.setVisibility(View.VISIBLE);

                        if(mData.getToilet().equalsIgnoreCase("tidak")){
                            mWc.setVisibility(View.GONE);
                        }else mWc.setVisibility(View.VISIBLE);

                        if(mData.getAtm().equalsIgnoreCase("tidak")){
                            mAtm.setVisibility(View.GONE);
                        }else mAtm.setVisibility(View.VISIBLE);

                        if(mData.getPompa().equalsIgnoreCase("tidak")){
                            mMini.setVisibility(View.GONE);
                        }else mMini.setVisibility(View.VISIBLE);


                        btnGo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Log.d(TAG, "button go click");
                                showNavigation(Double.valueOf(mData.getLatitude()), Double.valueOf(mData.getLongitude()));
                            }
                        });

                        mAtm.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Dialog dialogg = new Dialog(getContext());
                                dialogg.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialogg.setContentView(R.layout.pop_up_bank);

                                WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                                Window window = dialogg.getWindow();
                                layoutParams.copyFrom(window.getAttributes());
                                layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                                layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                dialogg.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                dialogg.setCancelable(true);
                                dialogg.setCanceledOnTouchOutside(true);
                                window.setAttributes(layoutParams);

                                ImageView imgBri, imgBni, imgBca, imgMandiri, imgMuamalat;

                                imgBri = dialogg.findViewById(R.id.logo_bri);
                                imgBni = dialogg.findViewById(R.id.logo_bni);
                                imgBca = dialogg.findViewById(R.id.logo_bca);
                                imgMandiri = dialogg.findViewById(R.id.logo_mandiri);
                                imgMuamalat = dialogg.findViewById(R.id.logo_muamalat);
                                if(mData.getBri().equals("tidak")){
                                    imgBri.setVisibility(View.GONE);
                                }if(mData.getBni().equals("tidak")){
                                    imgBni.setVisibility(View.GONE);
                                }if(mData.getBca().equals("tidak")){
                                    imgBca.setVisibility(View.GONE);
                                }if(mData.getMandiri().equals("tidak")){
                                    imgMandiri.setVisibility(View.GONE);
                                }if(mData.getMuamalat().equals("tidak")){
                                    imgMuamalat.setVisibility(View.GONE);
                                }

                                dialogg.show();
                            }
                        });

                        dialog.show();

                    }
                }




                return true;


            }
        });


        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker arg0) {


                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });


    }

    public void showNavigation(Double latitude, Double langitude) {

        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://ditu.google.cn/maps?f=d&source=s_d" +
                        "&saddr=" + latPos + "," + longPos +
                        "&daddr=" + latitude + "," + langitude +
                        "&hl=zh&t=m&dirflg=d"
                ));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK & Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        getContext().startActivity(intent);

    }


    protected synchronized void buildGoogleApiClient() {
        Log.d(TAG, "buildGoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    int i=0;
    @Override
    public void onLocationChanged(Location location) {
        //place marker at current position
        googleMap.clear();
        if (currLocationMarker != null) {
            currLocationMarker.remove();
        }
        latLng = new LatLng(location.getLatitude(), location.getLongitude());
        longPos = location.getLongitude();
        latPos = location.getLatitude();
        Log.d(TAG, valueOf(longPos) + "location changed");

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        currLocationMarker = googleMap.addMarker(markerOptions);

        Log.d(TAG, "Location Changed");


//        for (final dataPertamina data : listPertamina) {
//            googleMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(data.getLatitude()), Double.valueOf(data.getLongitude()))).title(data.getId()).snippet(data.getAlamat()).icon(BitmapDescriptorFactory.fromResource(R.mipmap.iconmark)));
//
//
//        }
        for (final dataPertamina data : listPertamina) {
            googleMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(data.getLatitude()), Double.parseDouble(data.getLongitude()))).title(data.getId()).snippet(data.getAlamat()).icon(BitmapDescriptorFactory.fromResource(R.mipmap.iconmark)));


        }


        //zoom to current position:

//        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));


        //If you only need one location, unregister the listener
        //LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        Log.d(TAG, "onConnected");
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation( //mendapatkan current location
                mGoogleApiClient);
        if (mLastLocation != null) {
            //place marker at current position
            googleMap.clear();
            latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());

            longPos = mLastLocation.getLongitude();
            latPos = mLastLocation.getLatitude();

            Log.d(TAG, valueOf(longPos) + "on connected");

            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title("Current Position");
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            currLocationMarker = googleMap.addMarker(markerOptions);

            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));

            loading.dismiss();
            dialogPertaminaTerdekat();
            for (final dataPertamina data : listPertamina) {
                googleMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(data.getLatitude()), Double.valueOf(data.getLongitude()))).title(data.getId()).snippet(data.getAlamat()).icon(BitmapDescriptorFactory.fromResource(R.mipmap.iconmark)));


            }


        }

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000); //5 seconds
        mLocationRequest.setFastestInterval(3000); //3 seconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        //mLocationRequest.setSmallestDisplacement(0.1F); //1/10 meter


        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private void dialogPertaminaTerdekat() {

        Dialog dialog = new Dialog(getContext());
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.pop_up_terdekat);

        DecimalFormat df = new DecimalFormat("#.#");

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        layoutParams.copyFrom(window.getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.setCanceledOnTouchOutside(true);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setAttributes(layoutParams);

        TextView jarak = window.findViewById(R.id.tv_jarak_dekat);
        TextView liter = window.findViewById(R.id.tv_liter_dekat);
        TextView id = window.findViewById(R.id.tv_id);
        TextView alamat = window.findViewById(R.id.tv_alamat);

        Button btnGo = window.findViewById(R.id.btn_go);

        final List<dataPertamina> dist = new ArrayList<>();


        Log.d(TAG, valueOf(Distance)+"sebelum ");

        double terdekat = 0, oriLat=0, oriLong=0;
        for(final dataPertamina d : listPertamina){

            try {
                oriLat = new Double(d.getLatitude());
                oriLong = new Double(d.getLongitude());

            }catch (NumberFormatException e){
                oriLat =0;
                oriLong=0;
            }

            LatLng from = new LatLng(latPos,longPos);
            LatLng to = new LatLng(oriLat,oriLong);

            //Calculating the distance in meters
            Double distance = SphericalUtil.computeDistanceBetween(from, to); // utility dari maps untuk menghitung jarak
            Distance = (distance*0.003)/2;
            Log.d(TAG, valueOf(Distance)+"sesudah ");

            dataPertamina l = new dataPertamina(d.getId(), d.getAlamat(), Distance, d.getLiter(), d.getLatitude(), d.getLongitude(), d.getPremium(), d.getPertamax(), d.getPertalite(), d.getSolar(), d.getMushallah(), d.getAtm(), d.getToilet(), d.getPompa(), d.getBri(), d.getBni(), d.getBca(), d.getMandiri(), d.getMuamalat());
            dist.add(l);

//            terdekat=0;
//
//            if(Distance >= terdekat){
//                terdekat = Distance;
//
//                Log.d(TAG, String.valueOf(terdekat)+"kondisi if ");
//
//
//            }


        }
        Log.d(TAG, String.valueOf(dist.size())+"sizex");
        if(dist.size() != 0){
            terdekat = dist.get(0).getJarak();
            for(int i =0; i<dist.size(); i++){
                if(dist.get(i).getJarak() <= terdekat){
                    terdekat = dist.get(i).getJarak();

                    Liter = (terdekat * 0.1);

                    liter.setText(valueOf(df.format(Liter)+" L"));
                    jarak.setText(valueOf(df.format(terdekat)+" KM"));
                    alamat.setText(dist.get(i).getAlamat());
                    id.setText(dist.get(i).getId());

                    final int finalI = i;
                    final int finalI1 = i;
                    btnGo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showNavigation(Double.valueOf(dist.get(finalI).getLatitude()), Double.valueOf(dist.get(finalI1).getLongitude()));

                        }
                    });
                }
            }
            dialog.show();
        }



    }




}
