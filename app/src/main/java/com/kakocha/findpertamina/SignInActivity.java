package com.kakocha.findpertamina;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignInActivity extends AppCompatActivity {

    EditText etIdSpbu, etPass;
    Button btnSignUp, btnMaster;
    String id, password;
    ImageView imgBack;
    AlertDialog.Builder builder;
    String url = UrlConfig.url + "login.php";
    String url_master = UrlConfig.url + "login_admin.php";
    String TAG = "SignInActivity";
    SessionManager session;
    String code, message;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        session = new SessionManager(getApplicationContext());

        etIdSpbu = (EditText) findViewById(R.id.et_id_spbu);
        etPass = (EditText) findViewById(R.id.et_pass_sign_in);
        btnSignUp = (Button) findViewById(R.id.btn_sign_in);
        btnMaster = (Button) findViewById(R.id.btn_sign_in_as_master);
        imgBack = (ImageView) findViewById(R.id.img_back);
        builder = new AlertDialog.Builder(SignInActivity.this);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signUpUser();

            }
        });

        btnMaster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signMaster();
            }
        });
    }

    private void signMaster() {
        id = etIdSpbu.getText().toString().trim().toLowerCase();
        password = etPass.getText().toString().trim().toLowerCase();

        if (id.equals("") || password.equals("")) {
            builder.setTitle("something went wrong ");
            builder.setMessage("please fill all the fields");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    etIdSpbu.setText("");
                    etPass.setText("");
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        } else {

            StringRequest request = new StringRequest(Request.Method.POST, url_master, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d(TAG, response);
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        code = jsonObject.getString("code");
                        message = jsonObject.getString("message");
                        Log.d(TAG, code);

                        if (code.equals("login_success")) {
                            builder.setTitle(code);
                            builder.setMessage(message);
                        } else if (code.equals("login_failed")) {
                            builder.setTitle(code);
                            builder.setMessage(message);

                        }
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (code.equals("login_success")) {
                                    session.createLoginSession(etIdSpbu.getText().toString(), etPass.getText().toString(), "master");
                                    Intent x = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(x);
                                    finish();
                                } else if (code.equals("login_failed")) {
                                    etIdSpbu.setText("");
                                    etPass.setText("");
                                }
                            }
                        });
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("id", id);
                    params.put("password", password);


                    return params;

                }
            };
            MySingleton.getInstance(SignInActivity.this).addToRequestQueue(request);

        }
    }

    void signUpUser() {
        id = etIdSpbu.getText().toString().trim().toLowerCase();
        password = etPass.getText().toString().trim().toLowerCase();

        if (id.equals("") || password.equals("")) {
            builder.setTitle("something went wrong ");
            builder.setMessage("please fill all the fields");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    etIdSpbu.setText("");
                    etPass.setText("");
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        } else {

            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d(TAG, response);
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        code = jsonObject.getString("code");
                        message = jsonObject.getString("message");
                        Log.d(TAG, code);

                        if (code.equals("login_success")) {
                            builder.setTitle(code);
                            builder.setMessage(message);
                        } else if (code.equals("login_failed")) {
                            builder.setTitle(code);
                            builder.setMessage(message);

                        }
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (code.equals("login_success")) {
                                    session.createLoginSession(etIdSpbu.getText().toString(), etPass.getText().toString(), "admin");
                                    Intent x = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(x);
                                    finish();
                                } else if (code.equals("login_failed")) {
                                    etIdSpbu.setText("");
                                    etPass.setText("");
                                }
                            }
                        });
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("id", id);
                    params.put("password", password);


                    return params;

                }
            };
            MySingleton.getInstance(SignInActivity.this).addToRequestQueue(request);

        }


    }



}
