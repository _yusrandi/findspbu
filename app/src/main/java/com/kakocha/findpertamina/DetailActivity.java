package com.kakocha.findpertamina;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.kakocha.findpertamina.model.dataPertamina;

public class DetailActivity extends AppCompatActivity {

    TextView tvId, tvAlamat, tvLiter, tvJarak, tvPremium, tvPertalite, tvPertamax, tvSolar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        tvId = (TextView) findViewById(R.id.tv_det_id);
        tvAlamat = (TextView) findViewById(R.id.tv_det_alamat);
        tvJarak = (TextView) findViewById(R.id.tv_det_jarak);
        tvLiter = (TextView) findViewById(R.id.tv_det_liter);
        tvPremium = (TextView) findViewById(R.id.tv_det_Premium);
        tvPertalite = (TextView) findViewById(R.id.tv_det_pertalite);
        tvPertamax = (TextView) findViewById(R.id.tv_det_pertamax);
        tvSolar = (TextView) findViewById(R.id.tv_det_solar);


        getData();

    }

    private void getData(){
        if(getIntent().getSerializableExtra("haha") != null){
            dataPertamina data = (dataPertamina) getIntent().getSerializableExtra("haha");

            tvId.setText(data.getId());
            tvAlamat.setText(data.getAlamat());
            tvLiter.setText(String.valueOf(data.getLiter())+" L");
            tvJarak.setText(String.valueOf(data.getJarak())+" KM");

            if(data.getPremium()==0){
                tvPremium.setText("Tidak tersedia");
            }else{
                tvPremium.setText("Rp. "+String.valueOf(data.getPremium()));
            }
            if(data.getPertamax()==0){
                tvPertamax.setText("Tidak tersedia");
            }else{
                tvPertamax.setText("Rp. "+String.valueOf(data.getPertamax()));
            }
            tvPertalite.setText("Rp. "+String.valueOf(data.getPertalite()));
            tvSolar.setText("Rp. "+String.valueOf(data.getSolar()));

        }
    }
}
