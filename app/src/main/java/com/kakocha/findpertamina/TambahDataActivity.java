package com.kakocha.findpertamina;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class TambahDataActivity extends AppCompatActivity {

    private LinearLayout imgLock, imgDone, imgBack;
    private static int PLACE_PICKER_REQUEST = 1;

    private EditText etId, etAlamat, etPremium, etSolar, etPertamax, etPertalite, etPass, etLatitude, etLongitude;
    SwitchCompat scPremium, scPertamax, scPertalite, scSolar, scMushallah, scAtm, scToilet, scMinimarket, scBri, scBni, scBca, scMandiri, scMuamalat;
    private String minimarket, atm, masjid, toilet, id, alamat, premium, solar, pertamax, pertalite, password, latitude, longitude, bri, bni, bca, mandiri, muamalat;

    private ImageView imgCoba;

    private String url = UrlConfig.url + "insert.php";
    private String TAG = "TambahDataActivity";

    private AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_data);
        imgLock = (LinearLayout) findViewById(R.id.img_location);
        etLatitude = (EditText) findViewById(R.id.et_lat);
        etLongitude = (EditText) findViewById(R.id.et_long);
        etId = (EditText) findViewById(R.id.et_id);
        etAlamat = (EditText) findViewById(R.id.et_alamat);
        etPremium = (EditText) findViewById(R.id.et_premium);
        etPertalite = (EditText) findViewById(R.id.et_pertalite);
        etPertamax = (EditText) findViewById(R.id.et_pertamax);
        etSolar = (EditText) findViewById(R.id.et_solar);
        etPass = (EditText) findViewById(R.id.et_pass);

        scPremium = (SwitchCompat) findViewById(R.id.sc_premium);
        scPertamax = (SwitchCompat) findViewById(R.id.sc_pertamax);
        scPertalite = (SwitchCompat) findViewById(R.id.sc_pertalite);
        scSolar = (SwitchCompat) findViewById(R.id.sc_solar);
        scMushallah = (SwitchCompat) findViewById(R.id.sc_mushallah);
        scAtm = (SwitchCompat) findViewById(R.id.sc_atm);
        scToilet = (SwitchCompat) findViewById(R.id.sc_toilet);
        scMinimarket = (SwitchCompat) findViewById(R.id.sc_minimarket);
        scBri = (SwitchCompat) findViewById(R.id.sc_bri);
        scBni = (SwitchCompat) findViewById(R.id.sc_bni);
        scBca = (SwitchCompat) findViewById(R.id.sc_bca);
        scMandiri = (SwitchCompat) findViewById(R.id.sc_mandiri);
        scMuamalat = (SwitchCompat) findViewById(R.id.sc_muamalat);

        imgDone = (LinearLayout) findViewById(R.id.img_done);
        builder = new AlertDialog.Builder(TambahDataActivity.this);

        getDataSwitchCompact();
        imgLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {

                    Intent intent = builder.build(TambahDataActivity.this);
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);

                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        imgDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData();
                insertData();
            }
        });

        imgBack = (LinearLayout) findViewById(R.id.img_back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void getData() {
        id = etId.getText().toString();
        alamat = etAlamat.getText().toString();
        premium = etPremium.getText().toString();
        pertalite = etPertalite.getText().toString();
        pertamax = etPertamax.getText().toString();
        solar = etSolar.getText().toString();
        password = etPass.getText().toString();
        latitude = etLatitude.getText().toString();
        longitude = etLongitude.getText().toString();


        if(scPremium.isChecked()){
            premium = etPremium.getText().toString();
        }else premium = "0";
        if(scPertamax.isChecked()){
            pertamax = etPertamax.getText().toString();
        }else pertamax = "0";
        if(scPertalite.isChecked()){
            pertalite = etPertalite.getText().toString();
        }else pertalite = "0";
        if(scSolar.isChecked()){
            solar = etSolar.getText().toString();
        }else solar = "0";

        if(scAtm.isChecked()){
            atm = "ya";
        }else atm ="tidak";
        if(scMinimarket.isChecked()){
            minimarket = "ya";
        }else minimarket="tidak";
        if(scToilet.isChecked()){
            toilet="ya";
        }else toilet="tidak";
        if(scMushallah.isChecked()){
            masjid = "ya";
        }else masjid ="tidak";

        if(scBri.isChecked()){
            bri = "ya";
        }else bri = "tidak";
        if(scBni.isChecked()){
            bni = "ya";
        }else bni = "tidak";
        if(scBca.isChecked()){
            bca = "ya";
        }else bca = "tidak";
        if(scMandiri.isChecked()){
            mandiri = "ya";
        }else mandiri = "tidak";
        if(scMuamalat.isChecked()){
            muamalat = "ya";
        }else muamalat = "tidak";
    }

    private void getDataSwitchCompact() {

        if(scPremium != null){
            scPremium.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    Log.d(TAG, "The Switch is " + (b ? "on" : "off"));

                    if(b){
                        Log.d(TAG, "The Switch is on");
                        etPremium.setEnabled(true);
                        premium = etPremium.getText().toString();

                    }else {
                        Log.d(TAG, "The Switch is off");
                        etPremium.setEnabled(false);
                        premium = "0";
                    }
                }
            });
        }
        if(scPertamax != null){
            scPertamax.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    Log.d(TAG, "The Switch is " + (b ? "on" : "off"));

                    if(b){
                        Log.d(TAG, "The Switch is on");
                        etPertamax.setEnabled(true);
                        pertamax = etPertamax.getText().toString();

                    }else {
                        Log.d(TAG, "The Switch is off");
                        etPertamax.setEnabled(false);
                        pertamax = "0";
                    }
                }
            });
        }

        if(scPertalite != null){
            scPertalite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    Log.d(TAG, "The Switch is " + (b ? "on" : "off"));

                    if(b){
                        Log.d(TAG, "The Switch is on");
                        etPertalite.setEnabled(true);
                        pertalite = etPertalite.getText().toString();

                    }else {
                        Log.d(TAG, "The Switch is off");
                        etPertalite.setEnabled(false);
                        pertalite = "0";
                    }
                }
            });
        }

        if(scSolar != null){
            scSolar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    Log.d(TAG, "The Switch is " + (b ? "on" : "off"));

                    if(b){
                        Log.d(TAG, "The Switch is on");
                        etSolar.setEnabled(true);
                        solar = etSolar.getText().toString();

                    }else {
                        Log.d(TAG, "The Switch is off");
                        etSolar.setEnabled(false);
                        solar = "0";
                    }
                }
            });
        }

        if(scMushallah != null){
            scMushallah.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        masjid = "ya";
                    }else {
                        masjid = "tidak";
                    }
                }
            });
        }
        if(scToilet != null){
            scToilet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        toilet = "ya";
                    }else {
                        toilet = "tidak";
                    }
                }
            });
        }
        if(scAtm != null){
            scAtm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        atm = "ya";
                    }else {
                        atm = "tidak";
                    }
                }
            });
        }
        if(scBri != null){
            scBri.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        bri = "ya";
                    }else {
                        bri = "tidak";
                    }
                }
            });
        }
        if(scBni != null){
            scBni.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        bni = "ya";
                    }else {
                        bni = "tidak";
                    }
                }
            });
        }
        if(scBca != null){
            scBca.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        bca = "ya";
                    }else {
                        bca = "tidak";
                    }
                }
            });
        }
        if(scMandiri != null){
            scMandiri.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        mandiri = "ya";
                    }else {
                        mandiri = "tidak";
                    }
                }
            });
        }
        if(scMuamalat != null){
            scMuamalat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        muamalat = "ya";
                    }else {
                        muamalat = "tidak";
                    }
                }
            });
        }

    }

    void insertData() {
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try {

                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);

                    String code = jsonObject.getString("code");
                    String message = jsonObject.getString("message");

                    builder.setTitle(code);
                    builder.setMessage(message);
                    displayAlert(code);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();


                Log.d(TAG, masjid + minimarket + id+ alamat + premium + pertamax+ pertalite+ solar+ masjid+ atm+minimarket+ password+"update data");

                params.put("id", id);
                params.put("alamat", alamat);
                params.put("premium", premium);
                params.put("pertamax", pertamax);
                params.put("pertalite", pertalite);
                params.put("solar", solar);
                params.put("mushallah", masjid);
                params.put("atm", atm);
                params.put("toilet", toilet);
                params.put("minimarket", minimarket);
                params.put("password", password);
                params.put("latitude", latitude);
                params.put("longitude", longitude);
                params.put("bri", bri);
                params.put("bni", bni);
                params.put("bca", bca);
                params.put("mandiri", mandiri);
                params.put("muamalat", muamalat);


                return params;

            }
        };
        MySingleton.getInstance(TambahDataActivity.this).addToRequestQueue(request);
    }

    void displayAlert(final String code) {
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (code.equals("add_success")) {
                    finish();
                    startActivity(new Intent(TambahDataActivity.this, MainActivity.class));
                }else if(code.equals("add_failed")){
                    builder.setTitle(code);
                    builder.setMessage("Please try again");

                }
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PLACE_PICKER_REQUEST){
            if(resultCode == RESULT_OK){

                Place place = PlacePicker.getPlace(TambahDataActivity.this, data);
                Double latitude = place.getLatLng().latitude;
                Double longitude = place.getLatLng().longitude;
                etId.setText(place.getName());
                etAlamat.setText(place.getAddress());
                etLatitude.setText(String.valueOf(latitude));
                etLongitude.setText(String.valueOf(longitude));

            }

        }
    }


}
