package com.kakocha.findpertamina;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kakocha.findpertamina.Fragment.ListFragment;
import com.kakocha.findpertamina.Fragment.MapsFragment;

import org.w3c.dom.Text;

import java.util.HashMap;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private ImageView imgMenu, imgMaps;
    private Toolbar toolbar;
    private TextView tvSignIn, tvUpdateData, haha, tvAddData;
    private String TAG = "MainActivity";
    SessionManager session;
    public static String idSpbu;
    LinearLayout layUpdate, layAdd;
    private String as;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        imgMenu = (ImageView) findViewById(R.id.img_menu);
        imgMaps = (ImageView) findViewById(R.id.img_map);
        drawerLayout = (DrawerLayout) findViewById(R.id.my_drawer);
        layUpdate = (LinearLayout) findViewById(R.id.layout_update);
        layAdd = (LinearLayout) findViewById(R.id.layout_add);
        tvAddData = (TextView) findViewById(R.id.tv_add_data);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.content, new MapsFragment()).commit();

        imgMaps.setOnClickListener(this);
        imgMaps.setTag("work");

        NavigationView navigationView = (NavigationView) findViewById(R.id.my_nav);


        View view = navigationView.getHeaderView(0);

        tvSignIn = (TextView) findViewById(R.id.tv_signIn_nav);
        tvUpdateData = (TextView) findViewById(R.id.tv_update_data);
        haha = (TextView) findViewById(R.id.tv_haha);

        tvSignIn.setText("Sign In");
        haha.setText("Masuk Mode Admin");
        layAdd.setVisibility(View.GONE);
        layUpdate.setVisibility(View.GONE);
        session = new SessionManager(getApplicationContext());
        Log.d(TAG, "User Login Status"+session.isLoggedIn());

        HashMap<String, String> user = session.getUserDetails();

        if(user != null){
            // name
            String id = user.get(SessionManager.KEY_ID);
            idSpbu = id;
            haha.setText(id);
            // email
            as = user.get(SessionManager.KEY_AS);
            System.out.println(as);

            if(as!=null){
                if(as.equalsIgnoreCase("admin")){
                    session.checkLogin(tvSignIn, haha, layUpdate, layAdd, as);
                    layUpdate.setVisibility(View.VISIBLE);
                    layAdd.setVisibility(View.GONE);
                }else if(as.equalsIgnoreCase("master")){
                    session.checkLogin(tvSignIn, haha, layUpdate, layAdd, as);
                    layUpdate.setVisibility(View.GONE);
                    layAdd.setVisibility(View.VISIBLE);
                }
            }
            Log.d(TAG, id+"id nya trakhir");
        }else {
            tvSignIn.setText("Sign In");
            haha.setText("Masuk Mode Admin");
            layUpdate.setVisibility(View.GONE);
            layAdd.setVisibility(View.GONE);
        }



        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        imgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        tvSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if("login".equals(tvSignNav.getTag())){
//                    startActivity(new Intent(MainActivity.this, SignInActivity.class));
//                    tvSignNav.setText("Sign Out");
//                    tvSignNav.setTag("logout");
//                }else if("logout".equals(tvSignNav.getTag())){
//                    session.logoutUser();
//                    tvSignNav.setText("Sign In");
//                    tvSignNav.setTag("login");
//                }
                if(session.isLoggedIn()){
                    tvSignIn.setText("Sign Out");
                    session.logoutUser(haha, tvSignIn);
                    session.checkLogin(tvSignIn, haha, layUpdate, layAdd, as);

                }else{
                    startActivity(new Intent(MainActivity.this, SignInActivity.class));
                    finish();
                    session.checkLogin(tvSignIn, haha, layUpdate, layAdd, as);
                }

            }
        });

        tvUpdateData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, DataDiriActivity.class));
            }
        });
        tvAddData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, TambahDataActivity.class));
            }
        });





    }

    @Override
    public void onClick(View view) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();


        if("work".equals(imgMaps.getTag())){
            transaction.replace(R.id.content, new ListFragment()).commit();
            imgMaps.setImageResource(R.drawable.ic_map_white_48dp);
            imgMaps.setTag("haha");
        }else if("haha".equals(imgMaps.getTag())) {
            transaction.replace(R.id.content, new MapsFragment()).commit();
            imgMaps.setImageResource(R.drawable.ic_list_white_48dp);
            imgMaps.setTag("work");
        }

    }






}
