package com.kakocha.findpertamina.Adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kakocha.findpertamina.DetailActivity;
import com.kakocha.findpertamina.Fragment.MapsFragment;
import com.kakocha.findpertamina.R;
import com.kakocha.findpertamina.model.dataPertamina;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PertaminaAdapter extends RecyclerView.Adapter<PertaminaAdapter.PertaminaViewHolder> {

    private Context context;
    private ArrayList<dataPertamina> listPertamina;
    private RecyclerView recyclerView;
    double longPos, latPos;
    ProgressDialog loading;

    public PertaminaAdapter(Context context, ArrayList<dataPertamina> listPertamina, RecyclerView recyclerView) {
        this.context = context;
        this.listPertamina = listPertamina;
        this.recyclerView = recyclerView;


    }


    @Override
    public PertaminaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pop_up_detail);

        longPos = MapsFragment.longPos;
        latPos = MapsFragment.latPos;

        final TextView tvId, tvAlamat, tvJarak, tvLiter, tvPremium, tvPertamax, tvPertalite, tvSolar;
        final Button btnGo;
        final LinearLayout mMasjid, mWc, mMinimarket, mAtm;

        tvId = dialog.findViewById(R.id.tv_id);
        tvAlamat = (TextView) dialog.findViewById(R.id.tv_alamat);
        tvJarak = (TextView) dialog.findViewById(R.id.tv_jarak);
        tvLiter = (TextView) dialog.findViewById(R.id.tv_liter);
        tvPremium = (TextView) dialog.findViewById(R.id.tv_det_Premium);
        tvPertalite = (TextView) dialog.findViewById(R.id.tv_det_pertalite);
        tvPertamax = (TextView) dialog.findViewById(R.id.tv_det_pertamax);
        tvSolar = (TextView) dialog.findViewById(R.id.tv_det_solar);
        btnGo = dialog.findViewById(R.id.btn_go_detail);
        mMasjid = dialog.findViewById(R.id.img_masjid);
        mWc = dialog.findViewById(R.id.img_wc);
        mMinimarket = dialog.findViewById(R.id.img_mini);
        mAtm = dialog.findViewById(R.id.img_atm);


        PertaminaViewHolder ViewHolder = new PertaminaViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = recyclerView.getChildLayoutPosition(view);
//                Intent intent = new Intent(context, DetailActivity.class);
//                intent.putExtra("haha", listPertamina.get(position) );
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                context.startActivity(intent);


                WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                Window window = dialog.getWindow();
                layoutParams.copyFrom(window.getAttributes());
                layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                window.setAttributes(layoutParams);

                final dataPertamina mData = listPertamina.get(position);


                tvId.setText(mData.getId());
                tvAlamat.setText(mData.getAlamat());
                tvJarak.setText(String.format(String.valueOf(mData.getJarak())) + " KM");
                tvLiter.setText(String.valueOf(mData.getLiter()) + " L");
                if(mData.getPremium()==0){
                    tvPremium.setText("Tidak tersedia");
                }else{
                    tvPremium.setText("Rp. " + String.valueOf(mData.getPremium()));
                }

                if(mData.getPertamax()==0){
                    tvPertamax.setText("Tidak tersedia");
                }else{
                    tvPertamax.setText("Rp. " + String.valueOf(mData.getPertamax()));
                }

                if(mData.getPertalite()==0){
                    tvPertalite.setText("Tidak tersedia");
                }else{
                    tvPertalite.setText("Rp. " + String.valueOf(mData.getPertalite()));
                }

                if(mData.getSolar()==0){
                    tvSolar.setText("Tidak tersedia");
                }else{
                    tvSolar.setText("Rp. " + String.valueOf(mData.getSolar()));
                }

                if(mData.getMushallah().equalsIgnoreCase("tidak")){
                    mMasjid.setVisibility(View.GONE);
                }else mMasjid.setVisibility(View.VISIBLE);

                if(mData.getToilet().equalsIgnoreCase("tidak")){
                    mWc.setVisibility(View.GONE);
                }else mWc.setVisibility(View.VISIBLE);

                if(mData.getAtm().equalsIgnoreCase("tidak")){
                    mAtm.setVisibility(View.GONE);
                }else mAtm.setVisibility(View.VISIBLE);

                if(mData.getPompa().equalsIgnoreCase("tidak")){
                    mMinimarket.setVisibility(View.GONE);
                }else mMinimarket.setVisibility(View.VISIBLE);

                btnGo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        showNavigation(Double.valueOf(mData.getLatitude()), Double.valueOf(mData.getLongitude()));
                    }
                });

                mAtm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Dialog dialogg = new Dialog(context);
                        dialogg.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialogg.setContentView(R.layout.pop_up_bank);

                        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                        Window window = dialogg.getWindow();
                        layoutParams.copyFrom(window.getAttributes());
                        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        dialogg.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialogg.setCancelable(true);
                        dialogg.setCanceledOnTouchOutside(true);
                        window.setAttributes(layoutParams);

                        ImageView imgBri, imgBni, imgBca, imgMandiri, imgMuamalat;

                        imgBri = dialogg.findViewById(R.id.logo_bri);
                        imgBni = dialogg.findViewById(R.id.logo_bni);
                        imgBca = dialogg.findViewById(R.id.logo_bca);
                        imgMandiri = dialogg.findViewById(R.id.logo_mandiri);
                        imgMuamalat = dialogg.findViewById(R.id.logo_muamalat);
                        if(mData.getBri().equals("tidak")){
                            imgBri.setVisibility(View.GONE);
                        }if(mData.getBni().equals("tidak")){
                            imgBni.setVisibility(View.GONE);
                        }if(mData.getBca().equals("tidak")){
                            imgBca.setVisibility(View.GONE);
                        }if(mData.getMandiri().equals("tidak")){
                            imgMandiri.setVisibility(View.GONE);
                        }if(mData.getMuamalat().equals("tidak")){
                            imgMuamalat.setVisibility(View.GONE);
                        }

                        dialogg.show();
                    }
                });
                dialog.show();

            }
        });
        return ViewHolder;


    }
    public void showNavigation(Double latitude, Double langitude) {

        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://ditu.google.cn/maps?f=d&source=s_d" +
                        "&saddr=" + latPos + "," + longPos +
                        "&daddr=" + latitude + "," + langitude +
                        "&hl=zh&t=m&dirflg=d"
                ));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK & Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        context.startActivity(intent);

    }

    @Override
    public void onBindViewHolder(PertaminaViewHolder holder, int position) {


        final dataPertamina data = listPertamina.get(position);

        holder.tvId.setText(data.getId());
        holder.tvAlamat.setText(data.getAlamat());
        holder.tvLiter.setText(String.valueOf(data.getLiter()) + " L");
        holder.tvJarak.setText(String.valueOf(data.getJarak()) + " KM");


    }

    @Override
    public int getItemCount() {
        if (listPertamina != null) {
            return listPertamina.size();
        }
        return 0;
    }


    //ViewHolder class
    public static class PertaminaViewHolder extends RecyclerView.ViewHolder {



        public TextView tvId, tvAlamat, tvLiter, tvJarak;

        public PertaminaViewHolder(View itemView) {
            super(itemView);

            tvId = itemView.findViewById(R.id.tv_id);
            tvAlamat = itemView.findViewById(R.id.tv_alamat);
            tvLiter = itemView.findViewById(R.id.tv_liter);
            tvJarak = itemView.findViewById(R.id.tv_jarak);

        }
    }
}
